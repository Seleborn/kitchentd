using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TowerStat : MonoBehaviour
{
  [SerializeField]   public int damage;// урон башни
  [SerializeField]  public float distance;// дальность башни
  [SerializeField]  public int cost;//стоимость башни
  [SerializeField] public float critChance;//шанс крита
  [SerializeField]   public bool splash;//возможность сплэш урона
  [SerializeField]   public float splashRadius;// радиус сплэш урона (о - если нет)
  [SerializeField]public float speed;//время между выстрелами в сек
  [SerializeField]   public bool stunChance;//возможность стана
     private float stunTime;//время стана (0 если стана нет)

     [SerializeField]public float StunTime
     {
          get => stunTime;
          set
          { if (stunChance)
                  stunTime = value;
              else stunTime = 0; } 
     } 
     private float armorPenetration; // в процентах 55,5 например

     [SerializeField]
     public float ArmorPenetration
     {
         get => armorPenetration;
         set {
             if (ArmorPenetration < 100)
                 armorPenetration = value;
             else armorPenetration = 100; }
         
     }
}

