using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChoisePaneManager : MonoBehaviour
{
    [SerializeField]  List<Image> choisePanelOnPlaces;
    public List<Image> ChoisePanelOnPlaces// кнопки на панели пушки 1-4
    {
        get => choisePanelOnPlaces;
        set => choisePanelOnPlaces = value;
    }
    [HideInInspector] public GameObject currentTowerPlace=null;
    void Start()
    {
        for (int i = 0; i < 4; i++)
        {
            var tempSprite = GameManager.Instance.TowersTypes[i].GetComponent<SpriteRenderer>();
            choisePanelOnPlaces[i].sprite = tempSprite.sprite;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
