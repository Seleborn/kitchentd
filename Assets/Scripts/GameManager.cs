using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;
public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }
    [HideInInspector]  [SerializeField] private RUDictionary ruDictionary;
    public RUDictionary RuDictionary => ruDictionary;
    [HideInInspector] [SerializeField] private ENDictionary enDictionary;
    public ENDictionary EnDictionary => enDictionary;
    [SerializeField] public List<GameObject> TowersTypes;
    [SerializeField] public ChoisePanelManager choisePanelManager;
    private Dictionary<GameObject, TowerStat> TowersTypesContainer;
   
    void Awake()
    {
        Instance = this;
        TowersTypesContainer=new Dictionary<GameObject, TowerStat>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void onClickTower1()
    {
        var currentPlaceTower = choisePanelManager.currentTowerPlace;
        var CurrentTower=Instantiate(TowersTypes[0], currentPlaceTower.transform.position, quaternion.identity);
        CurrentTower.SetActive(true);
        Destroy(currentPlaceTower);
        choisePanelManager.gameObject.SetActive(false);
    }
    public void onClickTower2()
    {
        var currentPlaceTower = choisePanelManager.currentTowerPlace;
        var CurrentTower=Instantiate(TowersTypes[1], currentPlaceTower.transform.position, quaternion.identity);
        CurrentTower.SetActive(true);
        Destroy(currentPlaceTower);
        choisePanelManager.gameObject.SetActive(false);
    }
    public void onClickTower3()
    {
        var currentPlaceTower = choisePanelManager.currentTowerPlace;
        var CurrentTower=Instantiate(TowersTypes[2], currentPlaceTower.transform.position, quaternion.identity);
        CurrentTower.SetActive(true);
        Destroy(currentPlaceTower);
        choisePanelManager.gameObject.SetActive(false);
    }
    public void onClickTower4()
    {
        var currentPlaceTower = choisePanelManager.currentTowerPlace;
        var CurrentTower=Instantiate(TowersTypes[3], currentPlaceTower.transform.position, quaternion.identity);
        CurrentTower.SetActive(true);
        Destroy(currentPlaceTower);
        choisePanelManager.gameObject.SetActive(false);
    }
}
