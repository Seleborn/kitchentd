using UnityEngine;
using UnityEngine.EventSystems;
  
public class IpointerTowerRange : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public GameObject popupWindowObject;
 
    private void Start()
    {
        popupWindowObject.SetActive(false);
    }
 
    private void Update()
    {
    }
    public void OnPointerEnter(PointerEventData eventData)
    {
        print("OnMouseEnter");
        popupWindowObject.SetActive(true);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        popupWindowObject.SetActive(false);
    }
}