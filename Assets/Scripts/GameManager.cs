using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }
    [SerializeField] public List<GameObject> TowersTypes;
    [SerializeField] private ChoisePaneManager _choisePaneManager;
   
    void Awake()
    {
        Instance = this;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void onClickTower1()
    {
        var currentPlaceTower = _choisePaneManager.currentTowerPlace;
        currentPlaceTower.GetComponent<SpriteRenderer>().sprite = _choisePaneManager.ChoisePanelOnPlaces[0].sprite;
    }
    public void onClickTower2()
    {
        var currentPlaceTower = _choisePaneManager.currentTowerPlace;
        currentPlaceTower.GetComponent<SpriteRenderer>().sprite = _choisePaneManager.ChoisePanelOnPlaces[1].sprite;
    }
    public void onClickTower3()
    {
        var currentPlaceTower = _choisePaneManager.currentTowerPlace;
        currentPlaceTower.GetComponent<SpriteRenderer>().sprite = _choisePaneManager.ChoisePanelOnPlaces[2].sprite;
    }
    public void onClickTower4()
    {
        var currentPlaceTower = _choisePaneManager.currentTowerPlace;
        currentPlaceTower.GetComponent<SpriteRenderer>().sprite = _choisePaneManager.ChoisePanelOnPlaces[3].sprite;
    }
}
