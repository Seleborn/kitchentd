using System;
using System.Collections;
using System.Collections.Generic;
using Microsoft.Unity.VisualStudio.Editor;
using UnityEngine;

public class PlaceTowwer : MonoBehaviour
{
    [SerializeField] private List<GameObject> towers;
    private GameObject currentTower;
    [SerializeField] private GameObject towersPanelChoise;

  
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public bool canPlaceTower()
    {
        return currentTower == null;
    }

    private void OnMouseUp()
    {
        if (canPlaceTower())
        {
           towersPanelChoise.SetActive(true);
           towersPanelChoise.GetComponent<ChoisePaneManager>().currentTowerPlace = gameObject;
        }
    }

    void towerPlacing()
    {
        
    }
}
